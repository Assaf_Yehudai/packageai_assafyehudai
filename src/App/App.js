/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import GeolocationStore from '../Common/Store/GeolocationStore';
import MainScreen from '../Modules/MainScreen';

import React from 'react';
import {Provider} from 'react-redux';
import {SafeAreaView} from 'react-native';

import GeolocationService from '../Common/Services/GeolocationService/GeoLocationService';

const App: () => React$Node = () => {

    return (
        <Provider store={GeolocationStore}>
            <GeolocationService/>
            <SafeAreaView style={{flex: 1}}>
                <MainScreen/>
            </SafeAreaView>
        </Provider>
    );
};

export default App;
