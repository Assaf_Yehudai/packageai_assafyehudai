import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

const initialState = {
    isRecordingLocation: false,
    coordinates: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'RECORD_BUTTON_TAPPED': {
            return {...state, isRecordingLocation: !state.isRecordingLocation};
        }

        case 'UPDATE_COORDINATES':
            const {newPosition} = action.payload;
            return {...state, coordinates: [...state.coordinates, newPosition]}


        default:
            return state;
    }
};

const GeolocationStore = createStore(reducer, applyMiddleware(thunk));
export default GeolocationStore;


