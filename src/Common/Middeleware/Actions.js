
export const recordButtonTapped = () => async (dispatch) => {

    dispatch({ type: 'RECORD_BUTTON_TAPPED' });
};

export const updateCoordinates = (newPosition) => async (dispatch) => {
    dispatch ({
        type: 'UPDATE_COORDINATES',
        payload : {newPosition: newPosition}
    })
}
