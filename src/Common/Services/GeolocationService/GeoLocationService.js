import {updateCoordinates} from '../../Middeleware/Actions';
import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Geolocation from 'react-native-geolocation-service';

const GeolocationService = () => {

    const [watchId, setWatchId] = useState(null);
    const dispatch = useDispatch();
    const isRecording = useSelector(state => state.isRecordingLocation);

    useEffect( recordingStateChanged, [isRecording]);

    function recordingStateChanged() {
        if (isRecording) {
            startRecording()
        } else {
            stopRecording()
        }
    }

    function startRecording() {

        if (hasLocationPermissionIOS) {
            setWatchId( Geolocation.watchPosition(
                (position) => {
                    const newPosition = creatNewPosition(position);
                    dispatch(updateCoordinates(newPosition));
                },
                (error) => {
                    console.log(error);
                }, {
                    interval: 30000,
                    distanceFilter: 0
                })
            )
        }
    }

    async function hasLocationPermissionIOS() {
        const status = await Geolocation.requestAuthorization('always');
        return status === 'granted';
    }

    function stopRecording() {
        console.log('stopping to record ---------------------------' + watchId)
        Geolocation.clearWatch(watchId);
        Geolocation.stopObserving();
    }

    function creatNewPosition(position) {
        const accuracy = position.coords.accuracy > 20;
        const {latitude, longitude} = position.coords
        const {timestamp} = position
        return {latitude: latitude, longitude: longitude, isAccurate: accuracy, timestamp: timestamp}
    }

    return null
}

export default GeolocationService;





// if (isRecording) {
//     setTimeout(addPoint1, 1000);
//     setTimeout(addPoint2, 3000);
//     setTimeout(addPoint3, 5000);
// } else {
//     console.log("Stopped Recording")
// }

// function addPoint1() {
//         const newPosition = {
//             latitude: '34.677',
//             longitude: '234.88',
//             timestamp: 'Feb 9, 13:53:03',
//             isAccurate: false
//         }
//         dispatch(updateCoordinates(newPosition))
//     }
//
//     function addPoint2() {
//         const newPosition = {
//             latitude: '89.677',
//             longitude: '754.88',
//             timestamp: 'Feb 8, 13:53:03',
//             isAccurate: false
//         }
//         dispatch(updateCoordinates(newPosition))
//     }
//
//     function addPoint3() {
//         const newPosition = {
//             latitude: '91.677',
//             longitude: '289.88',
//             timestamp: 'Feb 10, 13:53:03',
//             isAccurate: true
//         }
//         dispatch(updateCoordinates(newPosition))
//     }
