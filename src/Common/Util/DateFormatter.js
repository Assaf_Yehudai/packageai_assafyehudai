import dayjs from 'dayjs';

export function formatTimestamp(timestamp) {

    const date = dayjs(timestamp)
    return date.format('MMM D, YYYY \n HH:mm:ss')
}
