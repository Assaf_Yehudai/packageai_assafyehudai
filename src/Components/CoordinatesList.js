
import React from 'react';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import CoordinateCell from './CoordinateCell';

const CoordinatesList = () => {
    const coordinates = useSelector(state => state.coordinates)

    return <FlatList data={coordinates}
                     renderItem = {({item}) => <CoordinateCell cellItem={item} />}
                     keyExtractor={(item, index) => 'key'+index}/>
}

export default CoordinatesList;
