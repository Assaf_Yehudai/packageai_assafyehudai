import React from 'react';
import {View, Text} from 'react-native';
import {formatTimestamp} from '../Common/Util/DateFormatter';

const CoordinateCell = ({cellItem}) => {
    const {isAccurate, latitude, longitude, timestamp} = cellItem;
    return (
        <View style={styles.cellContainer(isAccurate)}>
            <Text style={styles.text}>{latitude.toFixed(2)}</Text>
            <Text style={styles.text}>{longitude.toFixed(2)}</Text>
            <Text style={styles.text}>{formatTimestamp(timestamp)}</Text>
        </View>
    )
}

export default CoordinateCell

const styles = {

    cellContainer(isAccurate) {
        return {
            flexDirection: 'row',
            height: 100,

            borderRadius: 10,
            borderColor: isAccurate ? '#ABAAAA' : '#F10505',
            borderWidth: 2,

            marginTop: 5,
            marginBottom: 5,
            marginRight: 8,
            marginLeft: 8,

            justifyContent: 'space-around',
            alignItems: 'center'
        }
    },

    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#000'
    }
}
