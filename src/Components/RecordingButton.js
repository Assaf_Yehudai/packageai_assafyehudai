import {recordButtonTapped} from '../Common/Middeleware/Actions';

import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

const RecordingButton = () => {
    const isRecording = useSelector((state) => state.isRecordingLocation);
    const dispatch = useDispatch();

    const title = isRecording ? 'Stop' : 'Start';
    const onButtonTapped = () => {
        dispatch(recordButtonTapped());
    };

    return (
        <TouchableOpacity style={styles.container} onPress={onButtonTapped}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    );
};


export default RecordingButton;

const styles = {

    container: {
        width: 200,
        height: 60,
        backgroundColor: '#ABAAAA',
        borderRadius: 5,

        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
      fontSize: 20
    }
}
