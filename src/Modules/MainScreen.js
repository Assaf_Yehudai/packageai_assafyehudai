import React from 'react';
import {View} from 'react-native';
import RecordingButton from '../Components/RecordingButton';
import CoordinatesList from '../Components/CoordinatesList';

const MainScreen = () => {
    return (
       <View style={styles.mainContainer}>
           <View style={styles.buttonContainer}>
               <RecordingButton/>
           </View>
           <View style={styles.listContainer}>
               <CoordinatesList/>
           </View>
       </View>
    );
}
export default MainScreen;

const styles = {

    mainContainer: {
        flex: 1
    },

    buttonContainer: {
        height: '25%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    listContainer: {
        flex: 1
    }
}
